﻿using System;
using System.IO;

namespace day3_1
{
    class Program
    {
        const char tree = '#';
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            string[] map = File.ReadAllLines("input.txt");
            char[,] grid = new char[map.Length, map[0].Length];

            for (int i = 0; i < map.Length; i++)
            {
                for (int j = 0; j < map[0].Length; j++)
                {
                    grid[i, j] = map[i].ToCharArray()[j];
                }
            }
            
        }
    }
}
