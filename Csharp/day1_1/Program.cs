﻿using System.IO;
using System;

namespace day1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            string[] text = File.ReadAllLines("input");
            int[] nb = new int[text.Length];
            for (int i = 0; i < text.Length; i++)
            {
                Console.WriteLine($"{i}. : {text[i]}");
                nb[i] = Int32.Parse(text[i]);
            }

            foreach (int n in nb)
            {
                int x = n;
                foreach (var m in nb)
                {
                    if (x + m == 2020)
                    {
                        Console.WriteLine("Found them ! Results are " + m + " and " + x);
                        Console.WriteLine("Final result is " + m * x);
                    }
                }
            }


        }
    }
}
