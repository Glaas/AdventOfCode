﻿using System.IO;
using System;

namespace day1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            string[] text = File.ReadAllLines("input");
            int[] nb = new int[text.Length];
            for (int i = 0; i < text.Length; i++)
            {
                nb[i] = Int32.Parse(text[i]);
            }

            foreach (int n in nb)
            {
                foreach (var m in nb)
                {
                    foreach (var p in nb)
                    {
                        if (n + m + p == 2020)
                        {
                            Console.WriteLine(n * m * p);
                        }
                    }
                }
            }


        }
    }
}
