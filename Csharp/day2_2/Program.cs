﻿using System.IO;
using System;

namespace day2_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            string[] text = File.ReadAllLines("input");
            char[] excludedChars = new char[] { '-', ' ', ':' };
            int nbOfTrues = 0;
            foreach (var item in text)
            {
                string[] slicedArr = item.Split(excludedChars, StringSplitOptions.RemoveEmptyEntries);

                int min = Int16.Parse(slicedArr[0]);
                int max = Int16.Parse(slicedArr[1]);
                char polChar = Char.Parse(slicedArr[2]);
                string password = slicedArr[3];
                /*Console.WriteLine(
                    $"Minimum is {min}\n" +
                    $"Maximum is {max}\n" +
                    $"Character is {polChar}\n" +
                    $"password is {password}\n");*/


                char check1 = password.ToCharArray()[min - 1];
                char check2 = password.ToCharArray()[max - 1];

                if (polChar == check1 && polChar == check2)
                {
                }
                else if (polChar != check1 && polChar != check2)
                {
                }
                else nbOfTrues++;




            }
            Console.WriteLine("nb of true is " + nbOfTrues);

        }
    }
}
