﻿using System.IO;
using System;

namespace day2_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            string[] text = File.ReadAllLines("input");
            char[] excludedChars = new char[] { '-', ' ', ':' };
            int nbOfTrues = 0;
            foreach (var item in text)
            {
                string[] slicedArr = item.Split(excludedChars, StringSplitOptions.RemoveEmptyEntries);

                int min = Int16.Parse(slicedArr[0]);
                int max = Int16.Parse(slicedArr[1]);
                char polChar = Char.Parse(slicedArr[2]);
                string password = slicedArr[3];
                /*Console.WriteLine(
                    $"Minimum is {min}\n" +
                    $"Maximum is {max}\n" +
                    $"Character is {polChar}\n" +
                    $"password is {password}\n");*/

                int nbOfOccurrences = password.Length - password.Replace(polChar.ToString(), "").Length;
                if (nbOfOccurrences >= min && nbOfOccurrences<= max) nbOfTrues++;




            }
            Console.WriteLine("nb of true is " + nbOfTrues);

        }
    }
}
